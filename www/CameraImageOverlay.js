"use strict";

var exec = require('cordova/exec');

/**
*
*	var config = {
	    url_image_overlay:"",
	    text_share:"",
	    
	    // only IOS
	    text_button_close_preview:"",
		
		// only Android
	    text_title_share:"",
	    text_button_retake:"",
	    text_button_share:"",
	    text_button_close_camera:"",
	    text_button_take:""
	  };
*
*
*/

var CameraImageOverlay = {
	setConfig:function(config){
		exec(null, null, "CameraImageOverlay", "setSettings", [config]);
	},
	start:function(){
		
		exec(null, null, "CameraImageOverlay", "start", []);

	}
};

module.exports = CameraImageOverlay;
