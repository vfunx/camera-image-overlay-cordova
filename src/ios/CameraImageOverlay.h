// Note that Xcode gets this line wrong.  You need to change "Cordova.h" to "CDV.h" as shown below.
#import <Cordova/CDV.h>

// Import the CustomCameraViewController class
#import "CustomCameraViewController.h"
#import "PreviewViewController.h"

@interface CameraImageOverlay : CDVPlugin

// Cordova command method
-(void) setSettings:(CDVInvokedUrlCommand*)command;
-(void) start:(CDVInvokedUrlCommand*)command;

// Create and override some properties and methods (these will be explained later)
-(void) capturedImageWithImage:(UIImage*)image;
-(void) cancelAction;
-(void) loadCamera;
-(void) loadCameraFromPreview;


@property (strong, nonatomic) CustomCameraViewController* overlay;
@property (strong, nonatomic) PreviewViewController *preview;
@property (strong, nonatomic) CDVInvokedUrlCommand* latestCommand;
@property (readwrite, assign) BOOL hasPendingOperation;
@property (strong, nonatomic) NSDictionary *config;

@property (strong, nonatomic) UIImage *imageOverlay;

//Texts
@property (strong, nonatomic) NSString *text_button_close_preview;
@property (strong, nonatomic) NSString *text_button_retake;
@property (strong, nonatomic) NSString *text_button_share;
@property (strong, nonatomic) NSString *text_button_close_camera;
@property (strong, nonatomic) NSString *text_button_take;
@property (strong, nonatomic) NSString *text_share;

@end