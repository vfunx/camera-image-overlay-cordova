#import "CameraImageOverlay.h"
#import "CustomCameraViewController.h"


@implementation CustomCameraViewController
// Entry point method
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	self.imageOverlay = (UIImageView*)[self.view viewWithTag:1];
	self.cancelButton = (UIButton*)[self.view viewWithTag:2];
	self.takeButton = (UIButton*)[self.view viewWithTag:3];


	UITapGestureRecognizer *clickCancel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelButtonPressed:)];
	[self.cancelButton addGestureRecognizer:clickCancel];

	UITapGestureRecognizer *clickTake = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhotoButtonPressed:)];
	[self.takeButton addGestureRecognizer:clickTake];
	

	if (self) {
		// Instantiate the UIImagePickerController instance
		self.picker = [[UIImagePickerController alloc] init];
	
		// Configure the UIImagePickerController instance
		self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		self.picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
		self.picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
		self.picker.showsCameraControls = NO;
	
		// Make us the delegate for the UIImagePickerController
		self.picker.delegate = self;
	
		// Set the frames to be full screen
		CGRect screenFrame = [[UIScreen mainScreen] bounds];
		self.view.frame = screenFrame;
		self.picker.view.frame = screenFrame;
	
		// Set this VC's view as the overlay view for the UIImagePickerController

		self.picker.cameraOverlayView = self.view;
	}

	return self;
}

-(void) setOverlay:(UIImage *)imageOverlay{

	self.imageOverlay.image = imageOverlay;

}
-(void) setTexts:(NSString *)cancelBtnText take:(NSString*)takeBtnText{

	[self.takeButton setTitle:takeBtnText forState:UIControlStateNormal];
	[self.cancelButton setTitle:cancelBtnText forState:UIControlStateNormal];

}
-(IBAction) cancelButtonPressed:(id)sender{
	NSLog(@"cancelButtonPressed");
	[self.plugin cancelAction];
}

// Action method.  This is like an event callback in JavaScript.
-(IBAction) takePhotoButtonPressed:(id)sender{
	// Call the takePicture method on the UIImagePickerController to capture the image.
	[self.picker takePicture];

}

// Delegate method.  UIImagePickerController will call this method as soon as the image captured above is ready to be processed.  This is also like an event callback in JavaScript.
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

	// Get a reference to the captured image
	UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];

	// Get a file path to save the JPEG
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	NSString* filename = @"cameraOverlay.jpg";
	NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:filename];

	// Get the image data (blocking; around 1 second)
	NSData* imageData = UIImageJPEGRepresentation(image, 0.5);

	// Write the data to the file
	[imageData writeToFile:imagePath atomically:YES];


	[self.plugin capturedImageWithImage:image];

}

@end