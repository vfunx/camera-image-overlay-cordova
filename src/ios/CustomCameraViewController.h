#import <UIKit/UIKit.h>

// We can't import the CameraImageOverlay class because it would make a circular reference, so "fake" the existence of the class like this:

@class CameraImageOverlay;

@interface CustomCameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>


-(IBAction) takePhotoButtonPressed:(id)sender;
-(void) setOverlay:(UIImage *)imageOverlay;
-(void) setTexts:(NSString *)cancelBtnText take:(NSString*)takeBtnText;

// Declare some properties (to be explained soon)
@property (strong, nonatomic) UIButton* cancelButton;
@property (strong, nonatomic) UIButton* takeButton;

@property (strong, nonatomic) CameraImageOverlay* plugin;
@property (strong, nonatomic) UIImagePickerController* picker;
@property (strong, nonatomic) UIImageView *imageOverlay;

@end