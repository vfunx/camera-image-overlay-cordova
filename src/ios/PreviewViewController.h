#import <UIKit/UIKit.h>

// We can't import the CameraImageOverlay class because it would make a circular reference, so "fake" the existence of the class like this:

@class CameraImageOverlay;

@interface PreviewViewController : UIViewController


-(void)setImageAndOverlay:(UIImage*)image overlayImage:(UIImage*)overlay;

-(void)setTexts:(NSString*)closeBtnTxt retake:(NSString*)retakeBtnTxt share:(NSString*)shareBtnTxt textShare:(NSString*)textShare;

@property (strong, nonatomic) UIView* viewImages;

@property (strong, nonatomic) UIButton* retake;
@property (strong, nonatomic) UIButton* shareButton;

@property (strong, nonatomic) CameraImageOverlay* plugin;

@property (strong, nonatomic) UIImageView *image;
@property (strong, nonatomic) UIImageView *imageOverlay;

@property (strong, nonatomic) NSString *text_share;


@end