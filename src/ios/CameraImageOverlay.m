#import "CameraImageOverlay.h"

@implementation CameraImageOverlay

// Cordova command method
-(void) setSettings:(CDVInvokedUrlCommand*)command{
	//[self.commandDelegate runInBackground:^{
		
		self.config = [command.arguments objectAtIndex:0];
		NSLog(@"setConfig --> %@", self.config);

		self.text_button_close_preview = [self.config valueForKey:@"text_button_close_preview"];
		self.text_button_retake = [self.config valueForKey:@"text_button_retake"];
		self.text_button_share = [self.config valueForKey:@"text_button_share"];
		self.text_button_close_camera = [self.config valueForKey:@"text_button_close_camera"];
		self.text_button_take = [self.config valueForKey:@"text_button_take"];
		self.text_share = [self.config valueForKey:@"text_share"];


		// Save the CDVInvokedUrlCommand as a property.  We will need it later.
		self.latestCommand = command;

		if(self.imageOverlay == nil){
			//[self.commandDelegate runInBackground:^{
				self.imageOverlay = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.config valueForKey:@"url_image_overlay"]]]];
			//}];
		}

		[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"] callbackId:self.latestCommand.callbackId];
		return;
	//}];
}
-(void) start:(CDVInvokedUrlCommand *)command {
	// Make the overlay view controller.
	// Set the hasPendingOperation field to prevent the webview from crashing
	self.hasPendingOperation = YES;
	[self loadCamera];

	//[self loadPreview:@"path"];

	//[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:imagePath] callbackId:self.latestCommand.callbackId];
	//[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[NSString stringWithFormat:@"%@/%@", @"file://", imagePath]] callbackId:self.latestCommand.callbackId];
}

// Method called by the overlay when the image is ready to be sent back to the web view
-(void) capturedImageWithImage:(UIImage*)image {
	
	// Unset the self.hasPendingOperation property
	self.hasPendingOperation = NO;

	[self.overlay.picker dismissViewControllerAnimated:YES completion:^{
		if(self.preview == nil){
			self.preview = [[PreviewViewController alloc] initWithNibName:@"PreviewViewController" bundle:nil];
		}


		[self.preview setImageAndOverlay:image overlayImage:self.imageOverlay];

		//send texts params
		[self.preview setTexts:(NSString*)self.text_button_close_preview retake:(NSString*)self.text_button_retake share:(NSString*)self.text_button_share textShare:(NSString*)self.text_share];
		
		self.preview.plugin = self;

		[self.viewController presentViewController:self.preview animated:YES completion:nil];
	}];

}


-(void) loadCameraFromPreview{

	[self.viewController dismissViewControllerAnimated:YES completion:^{
		[self loadCamera];
	}];

}

// loadCamera
-(void) loadCamera{
	

	if(self.overlay == nil){

		self.overlay = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
		self.overlay.plugin = self;

	}
	[self.overlay setTexts:(NSString *)self.text_button_close_camera take:(NSString*)self.text_button_take];
	[self.overlay setOverlay:self.imageOverlay];

	// Display the view.  This will "slide up" a modal view from the bottom of the screen.
	[self.viewController presentViewController:self.overlay.picker animated:YES completion:nil];

}

-(void) cancelAction{
	[self.viewController dismissViewControllerAnimated:YES completion:nil];
}

@end