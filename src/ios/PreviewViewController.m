#import "CameraImageOverlay.h"
#import "PreviewViewController.h"

@implementation PreviewViewController
// Entry point method
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	

	self.image = (UIImageView*)[self.view viewWithTag:1];
	self.imageOverlay = (UIImageView*)[self.view viewWithTag:2];
	self.retake = (UIButton*)[self.view viewWithTag:3];
	self.shareButton = (UIButton*)[self.view viewWithTag:4];
	self.viewImages = (UIView*)[self.view viewWithTag:6]; 


	UITapGestureRecognizer *clickRetake = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(retakeButtonPressed:)];
	[self.retake addGestureRecognizer:clickRetake];

	UITapGestureRecognizer *clickShare = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareButtonPressed:)];
	[self.shareButton addGestureRecognizer:clickShare];


	//UITapGestureRecognizer *clickCancel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelButtonPressed:)];
	//[closeButton.customView addGestureRecognizer:clickCancel];


	return self;
	
}
-(void)setImageAndOverlay:(UIImage*)image overlayImage:(UIImage*)overlay{

	//UIImage* flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationUpMirrored];
	self.image.transform = CGAffineTransformMakeScale(-1,1);
	self.image.image = image;

	self.imageOverlay.image = overlay;

}
-(void)setTexts:(NSString*)closeBtnTxt retake:(NSString*)retakeBtnTxt share:(NSString*)shareBtnTxt textShare:(NSString*)textShare{

	//Set navigation bar -----

	if(closeBtnTxt == nil || [closeBtnTxt isEqual:@""]){
		closeBtnTxt = @"Close";
	}

	UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];

	UINavigationItem *navItem = [[UINavigationItem alloc] init];

	UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:closeBtnTxt style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
	navItem.rightBarButtonItem = rightButton;

	navBar.items = @[ navItem ];

	[self.view addSubview:navBar];


	//set another buttons
	[self.retake setTitle:retakeBtnTxt forState:UIControlStateNormal];
	[self.shareButton setTitle:shareBtnTxt forState:UIControlStateNormal];

	//set text share
	self.text_share = textShare;


}
-(IBAction) retakeButtonPressed:(id)sender{
	NSLog(@"cancelButtonPressed");
	[self.plugin loadCameraFromPreview];
}

-(IBAction)cancelButtonPressed:(id)sender{
	[self.plugin cancelAction];
}

-(IBAction)shareButtonPressed:(id)sender{
	
	UIGraphicsBeginImageContext(CGSizeMake(self.viewImages.frame.size.width,self.viewImages.frame.size.height));
	CGContextRef context = UIGraphicsGetCurrentContext();
	[self.viewImages.layer renderInContext:context];
	UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	/*
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:@"imageOverlay.jpg"];

	// Get the image data (blocking; around 1 second)
	NSData* imageData = UIImageJPEGRepresentation(screenShot, 0.5);
	
	// Write the data to the file
	[imageData writeToFile:imagePath atomically:YES];

	UIImage *imgToShare = [UIImage imageNamed:imagePath];
	*/

	NSArray *itemsToShare = @[self.text_share, screenShot];
	UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];

	[self presentViewController:avc animated:YES completion:nil];

}



@end