package io.funx.plugins.cameraimageoverlay;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.LOG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.util.Log;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.funx.plugins.cameraimageoverlay.MainActivity;

/**
 * This class echoes a string called from JavaScript.
 */
public class CameraImageOverlay extends CordovaPlugin {
    private Context ctx;
    public static JSONObject config;
    public static Bitmap imageOverlay;
    
    // image to preview
    public static Bitmap imageCamera;

    // folder to save final image
    public static File mFolder;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        ctx = cordova.getActivity().getApplicationContext();
        
        if(action.equals("setSettings")){
            JSONObject config = new JSONObject(args.getString(0));
            this.setSettings(config);
            return true;
        }


        if (action.equals("start")){
            this.start();
            return true;
        }   
        return false;
    }

    private void start(){
        
        Intent i = new Intent(ctx, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);

    }
    private void setSettings(JSONObject config){
        Log.i("---->", config.toString());
        mFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+ "/cameraoverlay");

        if(!mFolder.exists()) {
            mFolder.mkdirs();
        }

        this.config = config;
        try{
            this.imageOverlay = this.getBitmapFromURL(config.getString("url_image_overlay"));
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    private static Bitmap getBitmapFromURL(String src) {

        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
            return null;
        }
        
    }


}
