package io.funx.plugins.cameraimageoverlay;


import android.content.res.Resources;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


import io.funx.plugins.cameraimageoverlay.CameraImageOverlay;

public class PreviewActivity extends Activity {
    
    String text_share;
    String button_share;
    String button_retake;
    String title_share;


    ImageView image;
    ImageView overlay;
    Button shareBtn;
    Button retakeBtn;
    private String package_name;
    private Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Hide the window title.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);


        package_name = getApplication().getPackageName();
        resources = getApplication().getResources();

        setContentView(resources.getIdentifier("preview", "layout", package_name));

        image = (ImageView)findViewById(resources.getIdentifier("image", "id", package_name));
        overlay = (ImageView)findViewById(resources.getIdentifier("overlay", "id", package_name));
        shareBtn = (Button)findViewById(resources.getIdentifier("share", "id", package_name));
        retakeBtn = (Button)findViewById(resources.getIdentifier("retake", "id", package_name));
            

        JSONObject info = CameraImageOverlay.config;
        try{
            
            text_share = info.getString("text_share");
            button_share = info.getString("text_button_share");
            button_retake = info.getString("text_button_retake");
            title_share = info.getString("text_title_share");

        }catch(JSONException e){
            e.printStackTrace();
        }
        

        shareBtn.setText(button_share);
        retakeBtn.setText(button_retake);


        retakeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onBackPressed();
            }
        });
        shareBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                share();
            }
        });


        image.setImageBitmap(CameraImageOverlay.imageCamera);
        overlay.setImageBitmap(CameraImageOverlay.imageOverlay);


    }

    protected void onResume() {
        super.onResume();
        //Setup the FrameLayout with the Camera Preview Screen

    }
    private String screenshot(){

        String image_path = CameraImageOverlay.mFolder.getAbsolutePath() + "cameraImageOverlay.jpg";

        File imgFile = new File(image_path);

        if(!imgFile.exists()) {
            try {
                imgFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FrameLayout view = (FrameLayout) findViewById(resources.getIdentifier("screenshot", "id", package_name));
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b = view.getDrawingCache();
        try {
            FileOutputStream out = new FileOutputStream(image_path);
            b.compress(Bitmap.CompressFormat.PNG, 95, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image_path;
    }
    private void share(){

        String image_path = screenshot();

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/png");
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(image_path)));
        share.putExtra(Intent.EXTRA_TEXT, text_share);
        startActivity(Intent.createChooser(share, title_share));
    }

    
}
